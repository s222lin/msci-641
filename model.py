
import torch
import torch.nn as nn
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler
from transformers import BertTokenizer, BertModel, BertForSequenceClassification
from transformers import AutoModel, BertTokenizerFast, get_linear_schedule_with_warmup

class BERT_Arch(nn.Module):

    def __init__(self, bert, num_hidden, num_classes):
        super(BERT_Arch, self).__init__()
        
        self.bert = bert        
        # dropout layer
        self.dropout = nn.Dropout(0.1)
        
        self.fc1 = nn.Linear(768, num_hidden)
        self.tanh = torch.nn.Tanh()

        #sigmoid activation function
        self.fc2 = nn.Linear(num_hidden, num_classes)
        self.sigmoid = torch.nn.Sigmoid()

    #define the forward pass
    def forward(self, sent_id, mask):
        
        #pass the inputs to the model  
        _, cls_hs = self.bert(sent_id, attention_mask=mask, return_dict=False)
      
        x = self.dropout(cls_hs)

        x = self.fc1(x)
        x = self.tanh(x)

        x = self.fc2(x)
        x = self.sigmoid(x)

        return x
    
def getModel(num_class, useOwnModel):
    hidden_size = int(num_class)  # Number of units in the bottleneck layer

    if useOwnModel == True:
        bert = AutoModel.from_pretrained('bert-base-uncased')
        model = BERT_Arch(bert, hidden_size, num_class)
        for param in model.bert.parameters():
            param.requires_grad = False
    else:
        model = BertForSequenceClassification.from_pretrained('bert-base-uncased', num_labels=num_class)
        for param in model.bert.parameters():
            param.requires_grad = False

    return model