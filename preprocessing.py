import math
import re
import numpy as np
import string

def removeContractions(text):

    mapping = {"ain't": "is not", "aren't": "are not","can't": "cannot", "'cause": "because", "could've": "could have", "couldn't": "could not", 
                       "didn't": "did not",  "doesn't": "does not", "don't": "do not", "hadn't": "had not", "hasn't": "has not", "haven't": "have not", 
                       "he'd": "he would","he'll": "he will", "he's": "he is", "how'd": "how did", "how'd'y": "how do you", "how'll": "how will", 
                       "how's": "how is",  "I'd": "I would", "I'd've": "I would have", "I'll": "I will", "I'll've": "I will have","I'm": "I am",
                       "I've": "I have", "i'd": "i would", "i'd've": "i would have", "i'll": "i will",  "i'll've": "i will have","i'm": "i am", 
                       "i've": "i have", "isn't": "is not", "it'd": "it would", "it'd've": "it would have", "it'll": "it will", "it'll've": "it will have",
                       "it's": "it is", "let's": "let us", "ma'am": "madam", "mayn't": "may not", "might've": "might have","mightn't": "might not",
                       "mightn't've": "might not have", "must've": "must have", "mustn't": "must not", "mustn't've": "must not have", "needn't": "need not", 
                       "needn't've": "need not have","o'clock": "of the clock", "oughtn't": "ought not", "oughtn't've": "ought not have", "shan't": "shall not",
                       "sha'n't": "shall not", "shan't've": "shall not have", "she'd": "she would", "she'd've": "she would have", "she'll": "she will", 
                       "she'll've": "she will have", "she's": "she is", "should've": "should have", "shouldn't": "should not", "shouldn't've": "should not have",
                       "so've": "so have","so's": "so as", "this's": "this is","that'd": "that would", "that'd've": "that would have", "that's": "that is",
                       "there'd": "there would", "there'd've": "there would have", "there's": "there is", "here's": "here is","they'd": "they would",
                       "they'd've": "they would have", "they'll": "they will", "they'll've": "they will have", "they're": "they are", "they've": "they have",
                       "to've": "to have", "wasn't": "was not", "we'd": "we would", "we'd've": "we would have", "we'll": "we will", "we'll've": "we will have",
                       "we're": "we are", "we've": "we have", "weren't": "were not", "what'll": "what will", "what'll've": "what will have", 
                       "what're": "what are",  "what's": "what is", "what've": "what have", "when's": "when is", "when've": "when have", "where'd": "where did",
                       "where's": "where is", "where've": "where have", "who'll": "who will", "who'll've": "who will have", "who's": "who is", 
                       "who've": "who have", "why's": "why is", "why've": "why have", "will've": "will have", "won't": "will not", "won't've": "will not have", 
                       "would've": "would have", "wouldn't": "would not", "wouldn't've": "would not have", "y'all": "you all", "y'all'd": "you all would",
                       "y'all'd've": "you all would have","y'all're": "you all are","y'all've": "you all have","you'd": "you would", "you'd've": "you would have",
                       "you'll": "you will", "you'll've": "you will have", "you're": "you are", "you've": "you have", 'u.s':'america', 'e.g':'for example'}


    specials = ["’", "‘", "´", "`"]
    for s in specials:
        text = text.replace(s, "'")
    for word in mapping.keys():
        if ""+word+"" in text:
            text = text.replace(""+word+"", ""+mapping[word]+"")
    #Remove Punctuations
    text = re.sub('[%s]' % re.escape(string.punctuation), '', text)
    # creating a space between a word and the punctuation following it
    # eg: "he is a boy." => "he is a boy ."
    text = re.sub(r"([?.!,¿])", r" \1 ", text)
    text = re.sub(r'[" "]+', " ", text)
    return text

def removeUnfrequentSkill(i_list, threshold):

    unique_set = set(i_list)
    unique_list = (list(unique_set))
    
    item_counts = {}
    for item in unique_set:
        item_counts[item] = i_list.count(item)

    r_list = []
    for key, value in item_counts.items():
        if value >= threshold and key != 'a':
            r_list.append(key)

    return r_list

def getSficfScoreDict(input_dict, categories, skills):
    tfidf_dict = dict()
    idf_dict = dict()

    for skill in skills:
        idf_dict[skill] = 0
        
    for cat in categories:
        tfidf_dict[cat] = dict()

    for cat in input_dict:
        for skill in skills:
            if input_dict[cat][skill] > 0:
                idf_dict[skill] = idf_dict[skill] + 1
    
    for skill in skills:
        if idf_dict[skill] == 0:
            idf_dict[skill] = math.log10(len(categories)/(1+idf_dict[skill]))
        else:
            idf_dict[skill] = math.log10(len(categories)/(idf_dict[skill]))
    
    for cat in input_dict:
        skill_sum = 0
        for skill in skills:
            skill_sum = input_dict[cat][skill] + skill_sum
        for skill in skills:
            tfidf_dict[cat][skill] = input_dict[cat][skill] / skill_sum
    
    for cat in tfidf_dict:
        for skill in tfidf_dict[cat]:
            tfidf_dict[cat][skill] = tfidf_dict[cat][skill] * idf_dict[skill]
    return tfidf_dict

def getAllSkills(data):
    all_skills = []
    for job in data['jobs']:
        all_skills = all_skills + list(job['skills_required'])
    return all_skills

def getCategories(data):
    categories = []
    for job in data['jobs']:
        if (job['job_category']!="not_available"):
            temp_cat = []
            for cat in  job['job_category']:
                temp_cat = temp_cat + cat.replace(' ','').split(",")
            categories = categories + temp_cat
    categories = np.unique(categories)
    categories = list(categories)
    
    return categories

def getCategoryDict(data, categories, skills):
    categories_dict = dict()
    for cat in categories:
        categories_dict[cat] = dict()
        for skill in skills:
            categories_dict[cat][skill] = 0
    
    for job in data['jobs']:
        if (job['job_category']!="not_available"):
            temp_cat = []
            for cat in  job['job_category']:
                temp_cat = temp_cat + cat.replace(' ','').split(",")
            for cat in temp_cat:
                for skill in job['skills_required']:
                    if skill in skills:
                        categories_dict[cat][skill] = categories_dict[cat][skill] + 1
    
    return categories_dict

def getTopSkills(categories_dict, categories, skills):
    tfidf_skill = getSficfScoreDict(categories_dict, categories, skills)
    topskills = []
    for cat in tfidf_skill:
        topskills = topskills + sorted(tfidf_skill[cat], key=tfidf_skill[cat].get, reverse=True)[:3]
    return topskills

def preprocessing(data, skills):
    withContractions = True
    #withContractions = False
    text_data = []
    label_data = []
    for job in data['jobs']:
        job_text = job['requirements_and_role'] + ' ' + job['job_requirements']
        
        if withContractions == False:
            job_text = removeContractions(job_text)
        
        skill_required = [float(0)] * len(skills)
        for skill in job['skills_required']:
            if skill in skills:
                skill_required[skills.index(skill)] = float(1)
        text_data.append(job_text)
        label_data.append(skill_required)

    return text_data, label_data