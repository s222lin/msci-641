import sys
import numpy as np
import pickle
import csv
import json
import torch
import torch.nn as nn
from torch.utils.data import TensorDataset, DataLoader, RandomSampler, SequentialSampler

from preprocessing import preprocessing, removeUnfrequentSkill, getAllSkills, getCategories, getCategoryDict, getTopSkills
from model import getModel

from sklearn.model_selection import train_test_split
from sklearn.metrics import precision_score
from sklearn.metrics import recall_score
from transformers import AutoModel, BertTokenizerFast, get_linear_schedule_with_warmup

def convertData(text_data, label_data):
    train_text, test_text, train_labels, test_labels = train_test_split(text_data, label_data, test_size=0.4, random_state=42,shuffle=True)
    val_text, test_text, val_labels, test_labels = train_test_split(test_text, test_labels, test_size=0.75, random_state=42,shuffle=True)

    tokenizer = BertTokenizerFast.from_pretrained('bert-base-uncased')

    # tokenize and encode sequences in the training set
    tokens_train = tokenizer.batch_encode_plus(
        train_text,
        max_length = 300,
        pad_to_max_length=True,
        truncation=True
    )

    # tokenize and encode sequences in the validation set
    tokens_val = tokenizer.batch_encode_plus(
        val_text,
        max_length = 300,
        pad_to_max_length=True,
        truncation=True
    )

    # tokenize and encode sequences in the test set
    tokens_test = tokenizer.batch_encode_plus(
        test_text,
        max_length = 300,
        pad_to_max_length=True,
        truncation=True
    )

    ## convert lists to tensors

    train_seq = torch.tensor(tokens_train['input_ids'])
    train_mask = torch.tensor(tokens_train['attention_mask'])
    train_y = torch.tensor(train_labels)

    val_seq = torch.tensor(tokens_val['input_ids'])
    val_mask = torch.tensor(tokens_val['attention_mask'])
    val_y = torch.tensor(val_labels)

    test_seq = torch.tensor(tokens_test['input_ids'])
    test_mask = torch.tensor(tokens_test['attention_mask'])
    test_y = torch.tensor(test_labels)

    #define a batch size
    batch_size = 64

    # wrap tensors
    train_data = TensorDataset(train_seq, train_mask, train_y)

    # sampler for sampling the data during training
    train_sampler = RandomSampler(train_data)

    # dataLoader for train set
    train_dataloader = DataLoader(train_data, sampler=train_sampler, batch_size=batch_size)

    # wrap tensors
    val_data = TensorDataset(val_seq, val_mask, val_y)

    # sampler for sampling the data during training
    val_sampler = SequentialSampler(val_data)

    # dataLoader for validation set
    val_dataloader = DataLoader(val_data, sampler = val_sampler, batch_size=batch_size)
    
    return train_dataloader, val_dataloader, test_seq, test_mask, test_y

# function to train the model
def train(model, device, train_dataloader, optimizer, scheduler):
    
    model.train()
    total_loss = 0
  
    # empty list to save
    # iterate over batches
    for step,batch in enumerate(train_dataloader):
        
        # progress update after every 50 batches.
        if step % 50 == 0 and not step == 0:
            print('  Batch {:>5,}  of  {:>5,}.'.format(step, len(train_dataloader)))
        
        # push the batch to gpu
        batch = [r.to(device) for r in batch]
 
        sent_id, mask, labels = batch

        # clear previously calculated gradients 
        model.zero_grad()       
        # get model predictions for the current batch
        outputs = model(sent_id, 
                    token_type_ids=None, 
                    attention_mask=mask, 
                    labels=labels)

        # compute the loss between actual and predicted values
        loss = outputs[0]
        
        # add on to the total loss
        total_loss = total_loss + loss.item()

        # backward pass to calculate the gradients
        loss.backward()

        # clip the the gradients to 1.0. It helps in preventing the exploding gradient problem
        torch.nn.utils.clip_grad_norm_(model.parameters(), 1.0)

        # update parameters
        optimizer.step()

        scheduler.step()

    # compute the training loss of the epoch
    avg_loss = total_loss / len(train_dataloader)
  

    #returns the loss and predictions
    return avg_loss

# function for evaluating the model
def evaluate(model, device, val_dataloader):
    
    print("\nEvaluating...")
  
    # deactivate dropout layers
    model.eval()

    total_loss, total_accuracy = 0, 0
    
    # iterate over batches
    for step,batch in enumerate(val_dataloader):

        # push the batch to gpu
        batch = [t.to(device) for t in batch]

        sent_id, mask, labels = batch

        # deactivate autograd
        with torch.no_grad():
            
            # model predictions
            outputs = model(sent_id, 
                            token_type_ids=None, 
                            attention_mask=mask, 
                            labels=labels)

            # compute the validation loss between actual and predicted values
            loss = outputs[0]

            total_loss = total_loss + loss.item()


    # compute the validation loss of the epoch
    avg_loss = total_loss / len(val_dataloader) 


    return avg_loss

def findThreshold(output, expected_num_skill):
    threshold = []
    for row in output:
        sorted_list = list(row)
        sorted_list.sort()
        threshold.append(sorted_list[-1*expected_num_skill])
    return sum(threshold) / len(threshold)

def formatOutput(output, threshold):
    for i in range(len(output)):
        for j in range(len(output[i])):
            if output[i][j] > threshold:
                output[i][j] = float(1)
            else:
                output[i][j] = float(0)
    return output

def findExpectedNumSkills(output_target):
    avg_skill = []
    for row in output_target:
        avg_skill.append(sum(row)) 
    return int(sum(avg_skill)/len(avg_skill))

def main():
    # load data
    with open('mycareersfuture.json') as json_file:
        data = json.load(json_file)
    
    skill_case = 0
    #skill_case = 1
    #skill_case = 2

    # all skills except ones appear less than 20 times
    if skill_case == 0:
        skills = removeUnfrequentSkill(getAllSkills(data),20)
    # top 20 skills
    elif skill_case == 1:
        skills = removeUnfrequentSkill(getAllSkills(data),3010)
    # top skills based on tfidf
    elif skill_case == 2:
        skills = getAllSkills(data)
        categores = getCategories(data)
        categories_dict = getCategoryDict(data, categores, skills)
        skills = getTopSkills(categories_dict, categores, skills)

    # get preprocessed text and labels
    text_data, label_data = preprocessing(data, skills)
    
    train_dataloader, val_dataloader, test_seq, test_mask, test_y = convertData(text_data, label_data)
    
    device = torch.device('cuda' if torch.cuda.is_available() else 'cpu')

    model = getModel(len(skills), False)
    model = model.to(device)

    # Define loss function and optimizer
    criterion = nn.BCEWithLogitsLoss()  # Binary Cross-Entropy Loss for multi-label classification
    optimizer = torch.optim.Adam(model.parameters(), lr=0.001, weight_decay=0.0001)


    # Training loop
    num_epochs = 8

    total_steps = len(train_dataloader) * num_epochs
    scheduler = get_linear_schedule_with_warmup(optimizer, 
                                                num_warmup_steps = 0, # Default value in run_glue.py
                                                num_training_steps = total_steps)
    # set initial loss to infinite
    best_valid_loss = float('inf')

    # empty lists to store training and validation loss of each epoch
    train_losses=[]
    valid_losses=[]


    #for each epoch
    for epoch in range(num_epochs):
        
        print('\n Epoch {:} / {:}'.format(epoch + 1, num_epochs))
        
        #train model
        train_loss = train(model, device, train_dataloader, optimizer, scheduler)
        
        #evaluate model
        valid_loss = evaluate(model, device, val_dataloader)
        
        #save the best model
        if valid_loss < best_valid_loss:
            best_valid_loss = valid_loss
            torch.save(model.state_dict(), 'saved_weights.pt')
        
        # append training and validation loss
        train_losses.append(train_loss)
        valid_losses.append(valid_loss)
        
        print(f'\nTraining Loss: {train_loss:.3f}')
        print(f'Validation Loss: {valid_loss:.3f}')
    
    #load weights of best model
    path = 'saved_weights_topskill.pt'
    model.load_state_dict(torch.load(path))

    # my PC crash if i do prediction in one run due to memory issue
    preds = []
    pred_size = 100
    # get predictions for test data
    with torch.no_grad():
        for i in range(0, len(test_seq),pred_size):
            if i + pred_size < len(test_seq):
                preds_t = model(test_seq[i:i+pred_size].to(device), test_mask[i:i+pred_size].to(device))
                preds.append(preds_t)


    output_target = test_y.detach().cpu().numpy()
    output_target = output_target[:-90]
    output = []
    for pred in preds:
        for row in pred[0].detach().cpu().numpy():
            output.append(row)
            output = np.array(output)

    exepected_num_skill = findExpectedNumSkills(output_target)
    threshold = findThreshold(output, exepected_num_skill)
    output = formatOutput(output, threshold)

    preds_rotate = np.rot90(output, k=3)
    labels_rotate = np.rot90(output_target, k=3)

    precision_list = []
    recall_list = []
    for i in range(len(preds_rotate)):
        precision_list.append(precision_score(labels_rotate[i], preds_rotate[i]))
        recall_list.append(recall_score(labels_rotate[i], preds_rotate[i]))
    return

if __name__ == "__main__":
    main()